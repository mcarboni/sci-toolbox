#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Importación de archivos csv a tabla main
"""
import pandas as pd
import sys
from model.sci import inicializarDB
from model.nombresParser import C1Evaluator

if len(sys.argv) < 2:
  print "Uso: python extrar_c1.py mysql://root@localhost/sci2015?charset=utf8"
  exit()

conexion = sys.argv[1]
# inicializamos la base de datos
# metadata.create_all()
engine = inicializarDB(conexion)

# vamos a buscar los datos de publicación y autor de la base
chunks = pd.read_sql('main', engine, chunksize=1, columns=['UT', 'C1', 'AU'])

for chunk in chunks:
  e = C1Evaluator()
  i = 0
  try:
    if (chunk['C1'][0] != None):
      c1Parseado = e.parse(chunk['C1'][0])
      elementos = []
      for key in c1Parseado:
        pais = key.split()[-1].upper()
        esNac = 1
        esInt = 0
        laUT = chunk['UT'][0].__str__()
        if pais != 'ARGENTINA':
          esNac = 0
          esInt = 1
        if not c1Parseado[key]:
          investigador = chunk['AU'][0]
          elementos.append({'UT': laUT, 'UTNC1': laUT+i.__str__(), 'C1': key, 'INVEST': investigador, 'NC1': i, 'PAIS': pais, 'INAC': esInt, 'NAC': esNac })
          #print "%s;%s;%s;%s;%d;%d;%d"%(chunk['UT'][0], key,chunk['AU'][0], pais, i, esNac, esInt)
          continue
        for investigador in c1Parseado[key]:
          elementos.append({'UT': laUT, 'UTNC1': laUT+i.__str__(), 'C1': key, 'INVEST': investigador, 'NC1': i, 'PAIS': pais, 'INAC': esInt, 'NAC': esNac })
          #print "%s;%s;%s;%s;%d;%d;%d"%(chunk['UT'][0], key, investigador, pais, i, esNac, esInt)
          i += 1
      #print "%s:%s" % ( chunk['UT'][0] , e.parse(chunk['C1'][0]) )
      pd.DataFrame(elementos).to_sql(name='c1', if_exists='append', con=engine, index=False,chunksize=1)
  except Exception as e:
    print "%s:%s" % (chunk['UT'][0], e)
    #exit(-1)

