#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Importación de archivos csv a tabla main
"""
import sys
import pandas as pd

from model.sci import inicializarDB

if len(sys.argv) < 2:
  print "Uso: python extrar_disc.py mysql://root@localhost/sci2015?charset=utf8"
  exit()

conexion = sys.argv[1]

# inicializamos la base de datos
# metadata.create_all()
engine = inicializarDB(conexion)

# vamos a buscar los datos de publicación y autor de la base
chunks = pd.read_sql('select * from main where sc is not null', engine, chunksize=100, columns=['UT', 'SC'])

for chunk in chunks:
  # separo las disciplinas
  disciplinas = chunk['SC'].str.split('|').apply(pd.Series, 1).stack()
  # emparejo los indices
  disciplinas.index = disciplinas.index.droplevel(-1)
  disciplinas.name = 'L1'
  nuevos = chunk.join(disciplinas).ix[:, ['UT', 'L1']]
  nuevos['L1'] = nuevos['L1'].str.strip()
  nuevos.to_sql(name='disc', if_exists='append', con=engine, index=False, chunksize=1)
