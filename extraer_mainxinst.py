#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Generación de las filas de MAINxINST

- busca las instituciones con su Regex y arma una lista de busqueda (search_list)
- recorre los elementos de la tabla C1 cuyo país es ARGENTINA
  - para el nombre de institucion en C1 los elementos de search_list con los que matchea
  - si matchea alguno
    - para cada match inserta una fila en MAINxINST
  - sino
    - inserta una fila con los datos de C1 y id de inst 9999 y codigo inst NONE   
"""
import sys
import pandas as pd
import re
from model.sci import inicializarDB
from model.nombresParser import ExpressionEvaluator

if len(sys.argv) < 2:
  print "Uso: python extrar_mainxinst.py mysql://root@localhost/sci2015?charset=utf8"
  exit()

conexion = sys.argv[1]

# inicializamos la base de datos
# metadata.create_all()
engine = inicializarDB(conexion)

# armamos los el par id, regex para cada institucion
search_list = []

# ojo con el natural join! Si el ID es 0 como con conicet no funca
# hago el join explícito usando el where!!!
ibs = pd.read_sql('select ins.ID, ins.ACRON, INST, RE from inst_busqueda ib, inst ins where ins.id = ib.id', engine, chunksize=1, columns=['ID','ACRON','INST','RE'])
for inst_busqueda in ibs:
  elist = inst_busqueda['RE'][0].split(';')

  # arma el string de la expresion regular
  pattern = '((.*?)'

  for i in range(0, len(elist)):
    pattern += elist[i] + '([A-Za-z]*?)'
    if (i != (len(elist) - 1)):
      pattern += '\\s'

  if len(inst_busqueda['ACRON'][0]) > 0:
    # le suma la busqueda por acronimo
    pattern += ')|(((.*?)\\s|^)' + inst_busqueda['ACRON'][0]+ '(\\s(.*?)|$))'
  else:
    pattern += ')'

  # agrega una expresion a la lista, junto con el id que la origina (id,re,inst)
  search_list.append((inst_busqueda['ID'][0], re.compile(pattern, re.I), inst_busqueda['INST'][0]))


# vamos a buscar las instituciones de la tabla C1
chunks = pd.read_sql('select * from c1 where NAC = 1', engine, chunksize=1, columns=['UT', 'ACRON', 'UTN1', 'NC1', 'C1'])
i = 0
for chunk in chunks:
  try:
    nombre = chunk['C1'][0]
    if nombre is not None:
      mainxinst = {}

      normalized = nombre.strip().upper().replace('(', ' ').replace(')',' ').replace('-',' ').replace(',',' ').replace(' DEL ', ' ').replace(' LA ',' ').replace(' LAS ',' ').replace(' EL ',' ').replace(' LOS ',' ').replace(' DE ',' ').replace(' & ',' ').replace(u'Á','A').replace(u'É','E').replace(u'Í','I').replace(u'Ó','O').replace(u'Ú','U')
      normalized = re.sub(' +', ' ', normalized)

      identified_lst = set()

      for (iid, regexp, inst) in search_list:
        if regexp.search(normalized):
          identified_lst.add((iid,inst))

      mainxinst['UT'] = chunk['UT'][0]
      mainxinst['UTNC1'] = chunk['UTNC1'][0]
      mainxinst['NC1'] = chunk['NC1'][0]
      mainxinst['C1'] = chunk['C1'][0]
      if len(identified_lst) > 0:
        n = 0
        for (iid,inst) in identified_lst:
            mainxinst['INST'] = inst
            mainxinst['ID'] = iid
            mainxinst['N'] = n
            n = n + 1
            df = pd.DataFrame([mainxinst])
            df.to_sql(name='mainxinst', if_exists='append', con=engine, index=False)
      else:
          mainxinst['INST'] = 'NONE'
          mainxinst['ID'] = 9999
          mainxinst['N'] = 0
          df = pd.DataFrame([mainxinst])
          df.to_sql(name='mainxinst', if_exists='append', con=engine, index=False)

      i = i + 1

      #df = pd.DataFrame([mainxinst])
      #df.to_sql(name='mainxinst', if_exists='append', con=engine, index=False)
      #mainxinst.to_sql(name='mainxinst', if_exists='append', con=engine, index=False)

  except Exception as e:
    print "%s:%s" % (chunk['UT'][0], e)
    exit(-1)

