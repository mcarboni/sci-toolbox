#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Importación de archivos csv a tabla main
"""
import sys
import pandas as pd
from sqlalchemy import exc
from model.sci import inicializarDB
from model.alias import columnAlias

if len(sys.argv) < 3:
  print "Uso: python importar.py nombre-de-archivo.csv mysql://root@localhost/sci2015?charset=utf8"
  exit()

verbose = "-v" in sys.argv or "--verbose" in sys.argv

archivo = sys.argv[1]
conexion = sys.argv[2]

engine = inicializarDB(conexion)

# abrimos el archivo y generamos un dataset
chunks = pd.read_csv(archivo, chunksize=1, encoding='utf-8-sig', index_col=False, header=1)

fila = 0
for chunk in chunks:
  fila += 1
  chunk.rename(columns=columnAlias, inplace=True)

  chunk['YR'] = chunk['PY'] # copio el campo año

  try:
    chunk.to_sql(name='main', if_exists='append', con=engine, index=False)
  except exc.IntegrityError as ex: # ya se inserto la fila
    if verbose:
      print "%s:%d:Integridad:%s"%(archivo, fila, ex.orig)
    pass 
  except exc.OperationalError as ex: # alguna constraint que no se verifica
    print "%s:%d:Revisar campos:%s"%(archivo, fila, ex.orig)
  except exc.SQLAlchemyError as ex:
    print "%s:%d:%s"%(archivo, fila, ex.orig)
  except Exception as ex:
    print "%s:%d:%s"%(archivo, fila, 'Revisar!')

