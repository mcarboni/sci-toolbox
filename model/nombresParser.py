#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import re
import collections

NOMBRE    = r'(?P<NOMBRE>\w[\w\s\-\'\.\,&]+)'
SEPARATOR = r'(?P<SEPARATOR>;)'
LPAREN    = r'(?P<LPAREN>\()'
RPAREN    = r'(?P<RPAREN>\))'
LBRACKET  = r'(?P<LBRACKET>\[)'
RBRACKET  = r'(?P<RBRACKET>\])'
WS        = r'(?P<WS>\s+)'

master_pat = re.compile('|'.join([NOMBRE, SEPARATOR, LPAREN, LBRACKET, RPAREN, RBRACKET, WS]))

Token = collections.namedtuple('Token', ['type','value'])

def generate_tokens(text):
    scanner = master_pat.scanner(text)
    for m in iter(scanner.match, None):
        tok = Token(m.lastgroup, m.group())
        if tok.type != 'WS':
            yield tok

# Parser
class ExpressionEvaluator:
    '''
    Implementation of a recursive descent parser.   Each method
    implements a single grammar rule.  Use the ._accept() method
    to test and accept the current lookahead token.  Use the ._expect()
    method to exactly match and discard the next token on on the input
    (or raise a SyntaxError if it doesn't match).
    '''

    def parse(self,text):
        self.tokens = generate_tokens(text)
        self.tok = None             # Last symbol consumed
        self.nexttok = None         # Next symbol tokenized
        self._advance()             # Load first lookahead token
        return self.expr()

    def _advance(self):
        'Advance one token ahead'
        self.tok, self.nexttok = self.nexttok, next(self.tokens, None)

    def _accept(self,toktype):
        'Test and consume the next token if it matches toktype'
        if self.nexttok and self.nexttok.type == toktype:
            self._advance()
            return True
        else:
            return False

    def _expect(self,toktype):
        'Consume next token if it matches toktype or raise SyntaxError'
        if not self._accept(toktype):
            raise SyntaxError('Expected ' + toktype)

    # Grammar rules follow

    def expr(self):
        "{ [investigador {; investigador} ] }* institucion { ; { [investigador {; investigador} ] }* institucion} * "
        exprval = None
        if self._accept('NOMBRE'):
            exprval = self.tok.value
        return exprval

class C1Evaluator(ExpressionEvaluator):
    '''
      Implementación de un parser para el campo c1
      Espera:
        [ investigador (; investigador)* ] institucion (; institucion)* (; [ investigador (; investigador)* ] institucion (; institucion)* )*

      Y retorna un diccionario de instituciones con sus investigadores
    '''

    # Grammar rules follow
    def expr(self):
        exprval = {}
        if self._accept('NOMBRE'):
          if (not self.tok.value in exprval):
            exprval[self.tok.value] = []
          exprval[self.tok.value]= []
        if self._accept('LBRACKET'):
           investigadores = []
           while self._accept('NOMBRE') or self._accept('SEPARATOR'):
             if (self.tok.type == 'NOMBRE'):
               #print self.tok.value
               investigadores.append(self.tok.value)
           self._accept('RBRACKET')
           self._accept('NOMBRE')

           if (not self.tok.value in exprval):
             exprval[self.tok.value] = []
           exprval[self.tok.value].extend(investigadores)
           while self._accept('SEPARATOR') or self._accept('NOMBRE'):
             if self.tok.type == 'NOMBRE':
               if (not self.tok.value in exprval):
                 exprval[self.tok.value] = []
               exprval[self.tok.value].extend(investigadores)
           exprval.update(self.expr())
        return exprval


"""
    ejemplo de parser de expresiones ariméticas básicas

    def expr(self):
        "expression ::= term { ('+'|'-') term }*"

        exprval = self.term()
        while self._accept('PLUS') or self._accept('MINUS'):
            op = self.tok.type
            right = self.term()
            if op == 'PLUS':
                exprval += right
            elif op == 'MINUS':
                exprval -= right
        return exprval

    def term(self):
        "term ::= factor { ('*'|'/') factor }*"

        termval = self.factor()
        while self._accept('TIMES') or self._accept('DIVIDE'):
            op = self.tok.type
            right = self.factor()
            if op == 'TIMES':
                termval *= right
            elif op == 'DIVIDE':
                termval /= right
        return termval

    def factor(self):
        "factor ::= NUM | ( expr )"

        if self._accept('NUM'):
            return int(self.tok.value)
        elif self._accept('LPAREN'):
            exprval = self.expr()
            self._expect('RPAREN')
            return exprval
        else:
            raise SyntaxError('Expected NUMBER or LPAREN')
"""
