#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
SO;Source
TI;Title
AU;Author
PD;Publication date
UT;
DB;Database
ID;Keywords Plus
DE;Author Keywords
AB;Abstract
AF;Full Author name(s)
C1;Linked Author names and addresses
OG;Organization
PY;Publication year
VL;Volume
OA;Organization address
SN;ISSN
LA;Language
PU;Publisher
DT;Document type
TC;Times cited
EM;Author email address
RP;Reprint address
NR;Number of references
PA;Publisher web address
WC;Subject category
SC;Standarized Subject category
CC;Current contents discipline
PI;Publisher city
PW;Publisher web address
CT;Conference Title
CY;Conference Date
CL;Conference Location
FU;Funding acknowledgement details
FX;Funding text
RS;Record source
CO;country / territory
PT;
BA;
BE;
GP;
BF;
CA;
YR;Publication // year
SE;
BS;
SP;
HO;
RI;
OI;
CR;
Z9;
BN;
J9;
JI;
I_S;Page number
PN;
SU;
SI;
MA;
BP;Begin Page
EP;End Page
AR;
DI;
D2;
PG;
P2;
TH;
GA;
"""
from sqlalchemy import Column, Integer, String, Table, Text, text, BigInteger
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa

engine = None
metadata = None

def inicializarDB(cnx):
	engine = sa.create_engine(cnx,pool_size=10, pool_recycle=7200)

	Base = declarative_base(engine)
	metadata = Base.metadata

	t_author = Table(
	    'author', metadata,
	    Column('UT', String(19, u'latin1_spanish_ci'), nullable=False, server_default=text("''")),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('SURNAME', String(255), nullable=False, server_default=text("''")),
	    Column('NAME', String(255), nullable=False, server_default=text("''"))
	)


	t_author_comp = Table(
	    'author_comp', metadata,
	    Column('UT', String(255), index=True),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('NAME', String(255), index=True)
	)


	t_c1 = Table(
	    'c1', metadata,
	    Column('UT', String(19, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('NC1', Integer, nullable=False, server_default=text("'0'")),
	    Column('UTNC1', Text(collation=u'latin1_spanish_ci'), nullable=False),
	    Column('C1', String(255, collation=u'latin1_spanish_ci'), nullable=False),
	    Column('PAIS', String(100, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    Column('INAC', Integer, nullable=False, server_default=text("'0'")),
	    Column('NAC', Integer, nullable=False, server_default=text("'0'")),
	    Column('INVEST', String(255), nullable=False)
	)


	t_cauthor = Table(
	    'cauthor', metadata,
	    Column('UT', String(19, u'latin1_spanish_ci'), nullable=False, server_default=text("''")),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('SURNAME', String(255, u'latin1_spanish_ci'), nullable=False),
	    Column('NAME', String(255, u'latin1_spanish_ci'), nullable=False)
	)


	t_disc = Table(
	    'disc', metadata,
	    Column('UT', String(19, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('L1', String(255, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    Column('L2', String(255), nullable=False, server_default=text("''"))
	)


	t_discagrup = Table(
	    'discagrup', metadata,
	    Column('AGRUP', Text(collation=u'latin1_spanish_ci')),
	    Column('L1', Text(collation=u'latin1_spanish_ci'))
	)


	class Inst(Base):
	    __tablename__ = 'inst'

	    ID = Column(Integer, primary_key=True, server_default=text("'0'"))
	    TIPO = Column(String(1, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))
	    NOMBRE = Column(String(255, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))
	    ACRON = Column(String(50, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))
	    INST = Column(String(7, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''"))
	    PROV = Column(String(50, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))
	    REG = Column(String(50, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))


	t_inst_busqueda = Table(
	    'inst_busqueda', metadata,
	    Column('ID', Integer, nullable=False, server_default=text("'0'")),
	    Column('TIPO', String(1, u'latin1_spanish_ci'), nullable=False, server_default=text("''")),
	    Column('NOMBRE', String(255, u'latin1_spanish_ci'), nullable=False, server_default=text("''")),
	    Column('ACRON', String(50, u'latin1_spanish_ci'), nullable=False, server_default=text("''")),
	    Column('RE', String(255, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))
	)


	class Main(Base):
	    __tablename__ = 'main'

	    YR = Column(String(4), nullable=False, index=True, server_default=text("''"))
	    PT = Column(String(1))
	    AU = Column(MEDIUMTEXT)
	    BA = Column(String(255, u'latin1_spanish_ci'))
	    BE = Column(String(255, u'latin1_spanish_ci'))
	    GP = Column(String(255, u'latin1_spanish_ci'))
	    AF = Column(MEDIUMTEXT, nullable=True)
	    BF = Column(Text)
	    CA = Column(String(255))
	    TI = Column(Text)
	    SO = Column(String(255))
	    SE = Column(String(255))
	    BS = Column(Text)
	    LA = Column(String(255))
	    DT = Column(String(255))
	    CT = Column(String(255, u'latin1_spanish_ci'), nullable=True)
	    CY = Column(String(255, u'latin1_spanish_ci'), nullable=True)
	    CL = Column(String(255, u'latin1_spanish_ci'), nullable=True)
	    SP = Column(String(255, u'latin1_spanish_ci'))
	    HO = Column(String(255, u'latin1_spanish_ci'))
	    DE = Column(Text)
	    ID = Column(Text)
	    AB = Column(Text)
	    C1 = Column(MEDIUMTEXT)
	    RP = Column(Text)
	    EM = Column(Text)
	    RI = Column(String(255), nullable=True, server_default=text("''"))
	    OI = Column(Text)
	    FU = Column(Text(1023, u'latin1_spanish_ci'))
	    FX = Column(Text(8191, u'latin1_spanish_ci'))
	    CR = Column(Text)
	    NR = Column(String(10))
	    TC = Column(Integer)
	    Z9 = Column(Integer)
	    PU = Column(String(255))
	    PI = Column(String(255))
	    PA = Column(String(255))
	    SN = Column(String(10))
	    BN = Column(String(255, u'latin1_spanish_ci'))
	    J9 = Column(String(255))
	    JI = Column(String(255))
	    PD = Column(String(255))
	    PY = Column(String(10), index=True)
	    VL = Column(String(255))
	    IS = Column(String(255))
	    PN = Column(String(255))
	    SU = Column(String(255))
	    SI = Column(String(255))
	    MA = Column(String(255, u'latin1_spanish_ci'))
	    BP = Column(String(255))
	    EP = Column(String(255))
	    AR = Column(String(255))
	    DI = Column(String(255, u'latin1_spanish_ci'))
	    D2 = Column(String(255, u'latin1_spanish_ci'))
	    PG = Column(String(255))
	    WC = Column(Text(collation=u'latin1_spanish_ci'))
	    SC = Column(Text(collation=u'latin1_spanish_ci'))
	    GA = Column(String(255))
	    UT = Column(String(50, u'latin1_spanish_ci'), primary_key=True, server_default=text("''"))
	    #UT = Column(BigInteger, primary_key=True)
	    OG = Column(Text)
	    OA = Column(Text)
	    DB = Column(String(255))
	    TH = Column(Text)
	    CC = Column(Text)
	    PW = Column(String(255))
	    RS = Column(String(50))
	    CO = Column(String(50))

	t_mainxinst = Table(
	    'mainxinst', metadata,
	    Column('UT', String(19, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('NC1', Integer, nullable=False, server_default=text("'0'")),
	    Column('UTNC1', String(25, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    Column('MEMBER', Integer, nullable=False, server_default=text("'0'")),
	    Column('N', Integer, nullable=False, server_default=text("'0'")),
	    Column('C1', String(255, collation=u'latin1_spanish_ci'), nullable=False),
	    Column('ID', Integer, nullable=False, index=True, server_default=text("'0'")),
	    Column('INST', String(255, u'latin1_spanish_ci'), nullable=False),
	    Column('UTISI', String(19, u'latin1_spanish_ci'), index=True)
	)


	t_temp_correc = Table(
	    'temp_correc', metadata,
	    Column('UT', String(19, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    #Column('UT', Integer, nullable=False, index=True),
	    Column('NC1', Integer, nullable=False, server_default=text("'0'")),
	    Column('UTNC1', String(25, u'latin1_spanish_ci'), nullable=False, index=True, server_default=text("''")),
	    Column('MEMBER', Integer, nullable=False, server_default=text("'0'")),
	    Column('N', Integer, nullable=False, server_default=text("'0'")),
	    Column('C1', String(255, collation=u'latin1_spanish_ci'), nullable=False),
	    Column('ID', Integer, nullable=False, index=True, server_default=text("'0'")),
	    Column('INST', String(7, u'latin1_spanish_ci'), nullable=False, server_default=text("''"))
	)

	# inicializamos la base de datos
	metadata.create_all()

	return engine

