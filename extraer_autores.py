#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Importación de archivos csv a tabla main
"""
import sys
import pandas as pd
from model.sci import inicializarDB

if len(sys.argv) < 2:
  print "Uso: python extrar_autores.py mysql://root@localhost/sci2015?charset=utf8"
  exit()

conexion = sys.argv[1]

# inicializamos la base de datos
# metadata.create_all()
engine = inicializarDB(conexion)

# vamos a buscar los datos de publicación y autor de la base
chunks = pd.read_sql('main', engine, chunksize=100, columns=['UT', 'AU'])

for chunk in chunks:
  # separo los autores
  autores = chunk['AU'].str.split('|').apply(pd.Series, 1).stack()
  # emparejo los indices
  autores.index = autores.index.droplevel(-1)
  autores.name = 'SURNAME'
  nuevos = chunk.join(autores).ix[:, ['UT', 'SURNAME']]

  autores.name = 'NAME'
  completos = chunk.join(autores).ix[:, ['UT', 'NAME']]
  completos['NAME'] = completos['NAME'].str.strip()
  completos.to_sql(name='author_comp', if_exists='append', con=engine, index=False)

  # guardo authors
  nuevos['NAME'] = nuevos['SURNAME'].apply(lambda x: x.split(",")[-1].strip())
  nuevos['SURNAME'] = nuevos['SURNAME'].apply(lambda x: x.split(",")[0].strip())
  nuevos.to_sql(name='author', if_exists='append', con=engine, index=False)

  # guardo cauthor
  nuevos['SURNAME'] = nuevos['SURNAME'].apply(lambda x: x.upper())
  nuevos.to_sql(name='cauthor', if_exists='append', con=engine, index=False)
